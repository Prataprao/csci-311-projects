/**
/ * @file CensusData.cpp   Represents census population data.
 * 
 * @brief
 *    This file contains all of the sorting functions and their helpers.
 * 
 * @author 
 * @date 
 */

#include "CensusData.h"
#include <string>
#include<utility>
using namespace std;

void CensusData::insertionSort(int type) {
	if(type==0)
	{
		for(int j=1;j<data.size();j++)
		{
			Record *key = data[j];
			int i=j-1;
			while (i>=0 && data[i]->population>key->population)
			{
				data[i+1]=data[i];
				i=i-1;
            }
			data[i+1]=key;
		}
	}
	else if(type==1)
	{
		for(int j=1;j<(data.size());j++)
		{
			Record *key = data[j];
			int i=j-1;
			while (i>=0 && *(data[i]->city)>*(key->city))
			{
				data[i+1]=data[i];
				i=i-1;
			}
			data[i+1]=key;
		}
	}
}

void CensusData::mergeSort(int type) {
	int p=0;
	int r=data.size();
	r=r-1;
	if(type==0)
		mSortPopulation(p,r);
	else if(type==1)
		mSortName(p,r);
}

void CensusData::mSortPopulation(int p, int r)
{
	int q;
	if (p<r)
		{
			q=(p+r)/2;
			mSortPopulation(p,q);
			mSortPopulation(q+1,r);
			mSortMergePopulation(p,q,r);
		}	
}

void CensusData::mSortName(int p, int r)
{
	int q;
	if (p<r)
		{
			q=(p+r)/2;
			mSortName(p,q);
			mSortName(q+1,r);
			mSortMergeName(p,q,r);
		}	
}

void CensusData::mSortMergePopulation(int p, int q, int r)
{
	int i,j,k;
	int n1=q-p+1;
	int n2=r-q;
	std::vector<Record*> mLeft(n1);
	std::vector<Record*> mRight(n2);
	for(i=0;i<n1;i++)
	{
		mLeft[i]=data[p+i];
	}
	for(j=0;j<n2;j++)
	{
		mRight[j]=data[q+j+1];
	}
	k=p;
	while(!mLeft.empty() || !mRight.empty())
	{
		if(!mLeft.empty() && !mRight.empty())
		{
			if (mLeft.front()->population <= mRight.front()->population)
			{
				data[k]=mLeft.front();
				k=k+1;
				mLeft.erase(mLeft.begin());
			}
			else
			{
				data[k]=mRight.front();
				k=k+1;
				mRight.erase(mRight.begin());
			}
		}
		else if(!mLeft.empty())
		{
			for (i=0;i<mLeft.size();i++)
			{
				data[k]=mLeft[i];
				k=k+1;
			}
			break;
		}
		else if(!mRight.empty())
		{
			for (j=0;j<mRight.size();j++)
			{
				data[k]=mRight[j];
				k=k+1;
			}
			break;
		}
	}
}

void CensusData::mSortMergeName(int p, int q, int r)
{
	int i,j,k;
	int n1=q-p+1;
	int n2=r-q;
	std::vector<Record*> mLeft(n1);
	std::vector<Record*> mRight(n2);
	for(i=0;i<n1;i++)
	{
		mLeft[i]=data[p+i];
	}
	for(j=0;j<n2;j++)
	{
		mRight[j]=data[q+j+1];
	}
	k=p;
	while(!mLeft.empty() || !mRight.empty())
	{
		if(!mLeft.empty() && !mRight.empty())
		{
			if (*(mLeft.front()->city) <= *(mRight.front()->city))
			{
				data[k]=mLeft.front();
				k=k+1;
				mLeft.erase(mLeft.begin());
			}
			else
			{
				data[k]=mRight.front();
				k=k+1;
				mRight.erase(mRight.begin());
			}
		}
		else if(!mLeft.empty())
		{
			for (i=0;i<mLeft.size();i++)
			{
				data[k]=mLeft[i];
				k=k+1;
			}
			break;
		}
		else if(!mRight.empty())
		{
			for (j=0;j<mRight.size();j++)
			{
				data[k]=mRight[j];
				k=k+1;
			}
			break;
		}
	}
}

void CensusData::quickSort(int type) {
	int p=0;
	int r=data.size();
	r=r-1;
	if(type==0)
		qSortPopulation(p,r);
	else if(type==1)
		qSortName(p,r);
}

void CensusData::qSortName(int p, int r)
{
	int q;
	if (p<r)
		{
			q=qSortPartitionName(p,r);
			qSortName(p,q-1);
			qSortName(q+1,r);
		}	
}

void CensusData::qSortPopulation(int p, int r)
{
	int pivot,i,j;
    Record *temp;
    if(p<r)
    {
        pivot=p;
        i=p;
        j=r;
        while(i<j)
        {
            while(data[i]<=data[pivot] && i<=r)
            i=i+1;
            while(data[j]>data[pivot] && j>=p)
            j=j-1;
            if(i<j)
            {
                swap(data[i],data[j]);
               // temp=data[i];
                //data[i]=data[j];
                //data[j]=temp;
            }
        }
        swap(data[j],data[pivot]);
        //temp=data[j];
        //data[j]=data[pivot];
        //data[pivot]=temp;
        qSortPopulation(p,j-1);
        qSortPopulation(j+1,r);
    }
}

int CensusData::qSortPartitionName(int p, int r)
{
	Record *x=data[r];
    Record *temp;
	int i=p-1;
	for (int j=p;j<data.size();j++)
	{
		if(*(data[j]->city)<*(x->city))
		{
			i=i+1;
            temp    = data[j];
            data[j] = data[i];
            data[i] = temp;
            //swap(data[i],data[j]);
		}
	}
    temp=data[r];
    data[r]=data[i+1];
    data[i+1]=temp;
    //swap(data[r],data[i+1]);
	return i+1;
}
