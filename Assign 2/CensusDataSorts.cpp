/**
/ * @file CensusDataSorts.cpp   Represents census population data sorts.
 * 
 * @brief
 *    This file contains all of the sorting functions and their helpers.
 * 
 * @author Prataprao Ughade
 * @date 11 Oct 2013
 */

#include "CensusData.h"
#include <string>
#include<utility>
using namespace std;

/**
/ * insertionSort
 * 
 * @brief
 *    This function contains the code for performing insertion sort on the given input data.
 */
void CensusData::insertionSort(int type) {
	if(type==0)															//to check if data to be sorted is population or name
	{
		for(int j=1;j<data.size();j++)
		{
			Record *key = data[j];
			int i=j-1;
			while (i>=0 && data[i]->population>key->population)			//sort the data in place by comparison
			{
				data[i+1]=data[i];
				i=i-1;
            }
			data[i+1]=key;
		}
	}
	else if(type==1)													//to check if data to be sorted is population or name
	{
		for(int j=1;j<(data.size());j++)
		{
			Record *key = data[j];
			int i=j-1;
			while (i>=0 && *(data[i]->city)>*(key->city))				//sort the data in place by comparison
			{
				data[i+1]=data[i];
				i=i-1;
			}
			data[i+1]=key;
		}
	}
}

/**
/ * mergeSort
 * 
 * @brief
 *    This function contains the code for performing merge sort on the given input data.
 */
void CensusData::mergeSort(int type) {
	int p=0;
	int r=data.size();
	r=r-1;
	if(type==0)															// if-else loop to check type of data being sorted
		mSortPopulation(p,r);
	else if(type==1)
		mSortName(p,r);
}

/**
/ * mSortPopulation
 * 
 * @brief
 *    This helper function contains the code for performing recursive merge sort on the given population data.
 */
void CensusData::mSortPopulation(int p, int r)
{
	int q;
	if (p<r)
		{
			q=(p+r)/2;													//variable q stores the midpoint at every recursive call
			mSortPopulation(p,q);										//recursive call to break down and sort data
			mSortPopulation(q+1,r);										//recursive call to break down and sort data
			mSortMergePopulation(p,q,r);								//call for merging and sorting of data 
		}	
}

/**
/ * mSortName
 * 
 * @brief
 *    This helper function contains the code for performing recursive merge sort on the given name data.
 */
void CensusData::mSortName(int p, int r)
{
	int q;
	if (p<r)
		{
			q=(p+r)/2;													//variable q stores the midpoint at every recursive call
			mSortName(p,q);												//recursive call to break down and sort data
			mSortName(q+1,r);											//recursive call to break down and sort data
			mSortMergeName(p,q,r);										//call for merging and sorting of data
		}	
}

/**
/ * mSortMergePopulation
 * 
 * @brief
 *    This helper function contains the code for performing merging and sort on the given population data.
 */
void CensusData::mSortMergePopulation(int p, int q, int r)
{
	int i,j,k;
	int n1=q-p+1;														//setting of limits for divided data
	int n2=r-q;
	std::vector<Record*> mLeft(n1);										//creation of left and right vectors to combine later
	std::vector<Record*> mRight(n2);
	for(i=0;i<n1;i++)
	{
		mLeft[i]=data[p+i];												//input of data to left of midpoint into mLeft vector
	}
	for(j=0;j<n2;j++)
	{
		mRight[j]=data[q+j+1];											//input of data to right of midpoint into mRight vector
	}
	k=p;
	while(!mLeft.empty() || !mRight.empty())
	{
		if(!mLeft.empty() && !mRight.empty())							// condition to check if both mLeft and mRight vectors are empty
		{
			if (mLeft.front()->population <= mRight.front()->population)//comparison and sorting between mLeft and mRight vectors to sort original data
			{
				data[k]=mLeft.front();
				k=k+1;
				mLeft.erase(mLeft.begin());
			}
			else
			{
				data[k]=mRight.front();
				k=k+1;
				mRight.erase(mRight.begin());
			}
		}
		else if(!mLeft.empty())											//if mLeft is not empty, we can directly sort those elements without checking mRight
		{
			for (i=0;i<mLeft.size();i++)
			{
				data[k]=mLeft[i];
				k=k+1;
			}
			break;
		}
		else if(!mRight.empty())										//if mRight is not empty, we can directly sort those elements without checking mLeft
		{
			for (j=0;j<mRight.size();j++)
			{
				data[k]=mRight[j];
				k=k+1;
			}
			break;
		}
	}
}

/**
/ * mSortMergeName
 * 
 * @brief
 *    This helper function contains the code for performing merging and sort on the given name data.
 */
void CensusData::mSortMergeName(int p, int q, int r)
{
	int i,j,k;
	int n1=q-p+1;														//setting of limits for divided data
	int n2=r-q;
	std::vector<Record*> mLeft(n1);
	std::vector<Record*> mRight(n2);									//creation of left and right vectors to combine later
	for(i=0;i<n1;i++)
	{
		mLeft[i]=data[p+i];												//input of data to left of midpoint into mLeft vector
	}
	for(j=0;j<n2;j++)
	{
		mRight[j]=data[q+j+1];											//input of data to right of midpoint into mRight vector
	}
	k=p;
	while(!mLeft.empty() || !mRight.empty())
	{
		if(!mLeft.empty() && !mRight.empty())							// condition to check if both mLeft and mRight vectors are empty
		{
			if (*(mLeft.front()->city) <= *(mRight.front()->city))		//comparison and sorting between mLeft and mRight vectors to sort original data
			{
				data[k]=mLeft.front();
				k=k+1;
				mLeft.erase(mLeft.begin());
			}
			else
			{
				data[k]=mRight.front();
				k=k+1;
				mRight.erase(mRight.begin());
			}
		}
		else if(!mLeft.empty())											//if mLeft is not empty, we can directly sort those elements without checking mRight
		{
			for (i=0;i<mLeft.size();i++)
			{
				data[k]=mLeft[i];
				k=k+1;
			}
			break;
		}
		else if(!mRight.empty())										//if mRight is not empty, we can directly sort those elements without checking mLeft
		{
			for (j=0;j<mRight.size();j++)
			{
				data[k]=mRight[j];
				k=k+1;
			}
			break;
		}
	}
}

/**
/ * quickSort
 * 
 * @brief
 *    This function contains the code for performing quick sort on the given input data.
 */
void CensusData::quickSort(int type)
{
	qSortPartition(0, (data.size() - 1), type);
}

/**
/ * qSortPartition
 * 
 * @brief
 *    This helper function contains the code for performing recursive quick sort on the given data.
 */
void CensusData::qSortPartition(int p, int r, int type)
{
	if (p < r)
	{
		if (type == 0)														//type check for population
		{
			int q = qSortPartitionPopulation(p, r);							//returns midpoint and sorts data at every call
			qSortPartition(p, (q - 1), 0);									//recursive calls to divide and then sort data
			qSortPartition((q + 1), r, 0);
		}
		else if (type == 1)													//type check for name
		{
			int q = qSortPartitionName(p, r);								//returns midpoint and sorts data at every call
			qSortPartition(p, (q - 1), 1);									//recursive calls to divide and then sort data
			qSortPartition((q + 1), r, 1);									
		}
	}
}

/**
/ * qSortPartitionPopulation
 * 
 * @brief
 *    This helper function contains the code for performing sorting on the given population data.
 *	  It returns the midpoint for further partition and recursive sorting.
 */
int CensusData::qSortPartitionPopulation(int p, int r)
{
	int x = r;
	Record *temp;
	std::vector<Record*>::iterator itor;									//new iterators to serve as random access pointers
	std::vector<Record*>::iterator key;
	int i = (p - 1);
	key = data.begin()+r;
	itor = data.begin()+p;
	for (int j = p; j < r; j++)
	{
		if (((*itor)->population) <= ((*key)->population))					//comparison of elements and sorting
		{
			i++;
			temp = data[j];													//swapping of necessary data
			data[j] = data[i];
			data[i] = temp;
		}
		itor++;
	}
	    temp = data[x];														//swapping of necessary data
	    data[x] = data[(i + 1)];
	    data[(i + 1)] = temp;
		return (i + 1);
}

/**
/ * qSortPartitionName
 * 
 * @brief
 *    This helper function contains the code for performing sorting on the given name data.
 *	  It returns the midpoint for further partition and recursive sorting.
 */
int CensusData::qSortPartitionName(int p, int r)
{
	int x = r;
    Record *temp;
    std::vector<Record*>::iterator itor;									//new iterators to serve as random access pointers
    std::vector<Record*>::iterator key;
	int i = (p - 1);
	key = data.begin()+r;
	itor = data.begin()+p;
	for (int j = p; j < r; j++)
	{
		if((*(*itor)->city) <= (*(*key)->city))								//comparison of elements and sorting
		{
			i++;
            temp = data[j];													//swapping of necessary data
            data[j] = data[i];
            data[i] = temp;
		}
		itor++;
	}
    temp = data[x];															//swapping of necessary data
    data[x] = data[(i + 1)];
    data[(i + 1)] = temp;
	return (i + 1);
}

