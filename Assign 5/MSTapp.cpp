/*
Prataprao Ughade

MSTapp.cpp

Contains the main function to start the MST application
*/

#include"MSTapp.h"
#include"Graph.h"
#include"Vertex.h"
#include<math.h>
#include<stdlib.h>

int main ()
{
  MSTapp l;
  l.start();
}


void MSTapp::start ()
{
  string vertex;
  string edge;
  int weight;
  Graph l;
  Minpriority min;

  while (cin.peek () != '\n')				//while not a new line, get input
    {
      cin >> vertex;
      l.add_vertex (vertex);			       //add vertex to adj list
      Vertex *p = new Vertex (vertex, "NIL", 100);	
      min.min_heap_insert (p);				//insert into pri queue
    }

  while (cin >> vertex)					//grab other lines of input
    {
      cin >> edge;
      cin >> weight;

      l.add_edge (vertex, edge, weight);
      l.add_edge (edge, vertex, weight);
    }

  std::vector < Vertex * >a = min.myq ();			//creating a copy of pri queue to use in Graph class.
  l.set_priority (a);

  l.MST_prim ();						//calling PRIM
}