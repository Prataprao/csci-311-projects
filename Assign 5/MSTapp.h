/*
Prataprao Ughade

MSTapp.h

Contains the class definition as header file to start the MST application
*/

#ifndef MSTapp_H
#define MSTapp_H

class MSTapp
{

public:
  void start ();

};

#endif