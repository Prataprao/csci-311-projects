#include"Graph.h"
using namespace std;

Graph::Graph ()
{
//
}

//copying vector to graph class.
void Graph::set_priority (vector < Vertex * >a)
{
  mine = a;
}

//inserting into outer map.
void Graph::add_vertex (string a)
{

  g.insert (make_pair (a, inmap ()));

}

//inserting to inner map
void Graph::add_edge (string parent, string edge, int weight)
{

  g[parent].insert (make_pair (edge, weight));

}


//extracting the min, making vector smaller.
Vertex *Graph::extract_min ()
{
  if (mine.size () == 0)
    return NULL;

  int size1 = mine.size () - 1;
  Vertex *min = mine[0];
  mine[0] = mine[size1];
  mine.pop_back ();
  min_heapify1 (0, size1 - 1);
  return min;
}


//prim's algorithm
void Graph::MST_prim ()
{

  int count = 0;

  int weight = 0;

  map < string, inmap >::iterator n;
  map < string, int >::iterator it;
  map < string, inmap >::iterator find;


  while (!mine.empty ())
    {

  //extracting
      	Vertex *u = extract_min ();
	if(u->m_key == 100)
		cout << u->m_vertex << " NIL 0" << endl;
	else
       	cout << u->m_vertex << " " << u -> m_pred << " " << u -> m_key << endl;

	//counting for printing purposes.
       for(int i=0; i < mine.size(); i++)
		count++;

//calculating weight (+100).
      weight = u->m_key + weight;

//finding the one we popped in adjacency list.
      find = g.find (u->m_vertex);

      //iterating through the adjacency list for the one we just popped off
      for (it = (*find).second.begin (); it != (*find).second.end (); it++)
        {
          //iterating through priority queue, decreasing key as necessary
          for (int i = 0; i < mine.size (); i++)
            {

              if (((*find).first == u->m_vertex && (*it).second < 100))
                {
                  decrease_key ((*it).first, u->m_vertex, (*it).second);
                }
            }
        }

     }

//by defauly keys have a weight of 100, so the first key has a weight of 100.
//must be accounted for!
	cout << weight - 100 << endl;

}

//similar to algorithm in book, modified for min-heapify
void Graph::min_heapify1 (int i, int size)
{

  Minpriority cx;

//left()
  int l = (2 * i) + 1;

//right()
  int r = (2 * i) + 2;

  int min;

  if (l < size && mine[l]->m_key > mine[i]->m_key)
    {
      min = l;
    }
  else
    {
      min = i;
    }

  if (r < size && mine[r]->m_key > mine[min]->m_key)
    {
      min = r;
    }
  if (min != i)
    {
      std::swap (mine[i], mine[min]);
      min_heapify1 (min, size);
    }
}


//sets key and parent for adjacent nodes freshly popped off queue.
void Graph::decrease_key (string tar, string par, int key)
{
  for (int i = 0; i < mine.size (); i++)
    if (mine[i]->m_vertex == tar && key < mine[i]->m_key)
      {
        mine[i]->m_key = key;
        mine[i]->m_pred = par;
      }

//calling heapsort to resort the priority queue.
    heapsort ();
    build_min_heap(size);

}


//similar to code in book, accounting for vector starting at 0.
void Graph::build_min_heap (int size)
{


  for (int i = (floor (mine.size () / 2) - 1); i >= 0; i--)
    min_heapify1 (i, size);


}

//similar to required algorithm, only vector's start at 0.
void Graph::heapsort ()
{


  size = mine.size ();

  build_min_heap (size);
  for (int i = size - 1; i >= 1; i--)
    {
      std::swap (mine[0], mine[i]);
      size--;

      min_heapify1 (0, size);
    }
}
