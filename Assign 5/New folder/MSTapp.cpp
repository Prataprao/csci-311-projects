#include"MSTapp.h"
#include"Graph.h"
#include"Vertex.h"
#include<math.h>
#include<stdlib.h>

int
main ()
{

  MSTapp l;

  l.start();

}


void
MSTapp::start ()
{

  string vertex;
  string edge;
  int weight;
  Graph l;
  Minpriority min;

  //while not a new line, get input
  while (cin.peek () != '\n')
    {

      cin >> vertex;

      //add vertex to adj list
      l.add_vertex (vertex);
      Vertex *p = new Vertex (vertex, "NIL", 100);

      //insert into pri queue
      min.min_heap_insert (p);


    }


  //grab other lines of input
  while (cin >> vertex)
    {

      cin >> edge;
      cin >> weight;


      l.add_edge (vertex, edge, weight);
      l.add_edge (edge, vertex, weight);

    }

//creating a copy of pri queue to use in Graph class.
  std::vector < Vertex * >a = min.myq ();
  l.set_priority (a);

//calling PRIM
  l.MST_prim ();

}