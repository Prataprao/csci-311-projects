
#include "MinPriority.h"
#include<iostream>
#include<math.h>
using namespace std;

Minpriority::Minpriority ()
{
//
}

//inserts into priority queue
void Minpriority::min_heap_insert (Vertex *a)
{
  q.push_back (a);
}

//similar to algorithm in book, modified for min-heapify
void Minpriority::min_heapify11 (int i, int size)
{

  Minpriority cx;

//left()
  int l = (2 * i) + 1;

//right()
  int r = (2 * i) + 2;

  int min;
    cout << l ;
    cout << mine[l]->m_key ;
    cout << mine[i]->m_key;
    cout << size;

  if (l < size && mine[l]->m_key > mine[i]->m_key)
    {

      min = l;

    }
  else

    {

      min = i;

    }

  if (r < size && mine[r]->m_key > mine[min]->m_key)
    {

      min = r;


    }
  if (min != i)
    {
      std::swap (mine[i], mine[min]);
      min_heapify11 (min, size);
    }
}


//sets key and parent for adjacent nodes freshly popped off queue.
void Minpriority::decrease_key1 (string tar, string par, int key)
{


  for (int i = 0; i < mine.size (); i++)
    if (mine[i]->m_vertex == tar && key < mine[i]->m_key)
      {
        mine[i]->m_key = key;
        mine[i]->m_pred = par;
      }

//calling heapsort to resort priority queue.
    heapsort1 ();
//  build_min_heap1(size);

}


//similar to code in book, accounting for vector starting at 0.
void Minpriority::build_min_heap1 (int size)
{
  for (int i = (floor (mine.size () / 2) - 1); i >= 0; i--)
    min_heapify11 (i, size);
}

//similar to book, only vector's start at 0.
void Minpriority::heapsort1 ()
{
  size = mine.size ();

  build_min_heap1 (size);
  for (int i = size - 1; i >= 1; i--)
    {
      std::swap (mine[0], mine[i]);
      size--;

      min_heapify11 (0, size);
    }
}
