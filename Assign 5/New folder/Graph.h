#ifndef GRAPH_H
#define GRAPH_H

#include<map>
#include<iostream>
#include "MinPriority.h"
#include "Vertex.h"
#include <math.h>
using namespace std;


class Graph
{
public:
  typedef std::vector < Vertex * >queue;
  queue mine;
  int size;
  void decrease_key (string, string, int);
  void min_heapify1 (int, int);
  void set_priority (vector < Vertex * >);
  void add_vertex (string);
  void add_edge (string, string, int);
  void print ();
  Graph ();
  void MST_prim ();
  Vertex *extract_min ();

private:
  void heapsort ();
  void build_min_heap (int);
  typedef map < string, int > inmap;
  typedef map < string, inmap > graph;
  graph g;

};

#endif