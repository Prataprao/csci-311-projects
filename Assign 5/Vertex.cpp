/*
Prataprao Ughade

Vertex.cpp

contains all of the functions of class vertex, specifically
its constructor
*/

#include"Vertex.h"

Vertex::Vertex (string vertex, string pred, int key)		//creates vertex object
{
  m_vertex = vertex;
  m_pred = pred;
  m_key = key;
}