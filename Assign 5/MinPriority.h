/*
Prataprao Ughade

MinPriority.h

Contains the headers of functions for MinPriority.cpp file
*/

#ifndef MINPRIORITY_H
#define MINPRIORITY_H
#include "Vertex.h"
#include <vector>

class Minpriority
{

public:

  typedef std::vector<Vertex*> queue;
  Minpriority();
  int size;
  queue q;
  queue mine;
  queue myq() {return q;};
  void min_heap_insert(Vertex*);
  void min_heapify11 (int, int);
  void decrease_key1 (string, string,int);

private:

  void build_min_heap1 (int);
  void heapsort1();

};

#endif
