/**
 * @file DoublyLinkedList.cpp   Implementation of a Doubly Linked List class for a music playist.
 *
 * @brief
 *    Implementation of a doubly linked list for a music playlist. A doubly linked list is
 *    used to store the song names.
 *
 * @author Prataprao Ughade
 * @date 09/13/13
 */

#include <iostream>
#include <string>
#include "DoublyLinkedList.h"

using namespace std;

/**
 * Parametrized constructor declaration for node.
 *
 * Initialize members of the nested node class for using parametrized passing
 * of value.
 */
DoublyLinkedList::dnode::dnode(string &s)					//parametrized node constructor
{
   prev=NULL;
   next=NULL;
   data = new string;
   data->assign(s);
}

/**
 * Simple constructor declaration for node.
 *
 * Initialize members of the nested node class in doubly linked class
 * for using pointers of nodes.
 */
DoublyLinkedList::dnode::dnode()							//node constructor
{
   prev=NULL;
   next=NULL;
   data=NULL;
}

/**
 * Simple constructor declaration for class DoublyLinkedList.
 *
 * Initialize members of the doubly linked list class for using pointers
 * of nodes in the list like first, last and present node.
 */
DoublyLinkedList::DoublyLinkedList()						//primary constructor
{
    head=NULL;
    curr=NULL;
    tail=NULL;
}

/**
 * Function to append song at end of list.
 *
 * This function checks if the playlist is empty, then adds a new song
 * or if a playlist of one or more nodes is present, adds a new node
 * at the end of this playlist. It also takes care of assigning head,
 * current or tail pointers appropriately.
 */
void DoublyLinkedList::append(string &s)
{
    dnode *q,*n;

    if (empty())
    {
        q=new dnode;										//create a new node
        q->prev=NULL;
        q->data=new string;
        q->data->assign(s);
        q->next=NULL;
        curr=q;
        head=q;
        tail=q;
    }
    else
    {
        n=new dnode;										//add a new node at the end
        n->data=new string;
        n->data->assign(s);
        n->next=NULL;
        n->prev=tail;
        tail->next=n;
        curr=n;
        tail=n;
    }
}

/**
 * Function(boolean) to check if list is empty.
 *
 * This function checks if the playlist is empty and serves other functions
 * like 'remove' or 'append' to make the right actions possible. It returns 
 * boolean values 'true' or 'false'.
 */
bool DoublyLinkedList::empty()
{
    if(head==NULL)											
	{
		curr=NULL;											//taking care of boundary conditions
		tail=NULL;
        return true;
	}
    else
        return false;
}

/**
 * Function to add specified song before the current song in playlist.
 *
 * Based on whether the playlist is empty, the current song is the first
 * song or otherwise, this function inserts a new song as a node to the
 * playlist and handles the pointer assignment as appropriate for the 
 * position of the newly inserted song. The new song becomes the current song.
 */
void DoublyLinkedList::insertBefore(string &s)
{
    dnode *newnode=new dnode(s);

    if(curr==head)											//to insert new node at beginning of list
    {
        newnode->prev=NULL;									//assigning links to new node
        newnode->next=curr;
        curr->prev=newnode;									//make new node the head node
        curr=newnode;
        head=newnode;
    }
    else if(empty())										//to insert if list is empty
    {
        append(s);
    }
    else													//to insert node intermediately
    {
        newnode->prev=curr->prev;							//assigning links to new node
        newnode->next=curr;
        curr->prev->next=newnode;							//inserting new node between two nodes
        curr->prev=newnode;
        curr=newnode;
    }
}

/**
 * Function to add specified song after the current song in playlist.
 *
 * Based on whether the playlist is empty, the current song is the last
 * song or otherwise, this function inserts a new song as a node to the
 * playlist and handles the pointer assignment as appropriate for the 
 * position of the newly inserted song. The new song becomes the current song.
 */
void DoublyLinkedList::insertAfter(string &s)
{
    dnode *newnode=new dnode(s);

    if(curr==tail)											//to insert new node at end of list
    {
        append(s);
    }
    else if(empty())										//to insert if list is empty
    {
        append(s);
    }
    else													//to insert node intermediately
    {
        newnode->next=curr->next;							//assigning links to new node
        newnode->prev=curr;
        curr->next->prev=newnode;							//inserting new node between two nodes
        curr->next=newnode;
        curr=newnode;
    }
}

/**
 * Function to point to first song in playlist.
 *
 * This function checks the boundary conditions for an empty playlist,
 * and if not, it assigns the current pointer to the first song in
 * the playlist.
 */
void DoublyLinkedList::begin()
{
	if(!empty())
	curr=head;
}

/**
 * Function to point to last song in playlist.
 *
 * This function checks the boundary conditions for an empty playlist,
 * and if not, it assigns the current pointer to the last song in
 * the playlist.
 */
void DoublyLinkedList::end()
{
	if(!empty())
	curr=tail;
}

/**
 * Function to delete the specified song if present, in playlist.
 *
 * The remove function takes a reference string and using find function,
 * locates that string in the playlist. If found, based on its position, the
 * node pointers are appropriately reassigned and the specified song(node) is 
 * deleted.
 */
void DoublyLinkedList::remove(string &s)
{
    dnode *q;
	if(find(s))
	{
		if(curr!=NULL && curr==tail && curr==head)			//if only one node needs to be deleted
		{
			delete curr;									//free memory by deleting node
			head=NULL;
			tail=NULL;
		}
	    else if(curr!=NULL && curr==head)					//for deleting the first node
		{
			q=curr;
			curr=curr->next;
			curr->prev=NULL;
			head=curr;
			delete q;										//free memory by deleting node
		}
		else if(curr!=NULL && curr==tail)					//for deleting the last node
		{
			q=curr;
			curr=curr->prev;
			curr->next=NULL;
			tail=curr;
			delete q;										//free the memory by deleting node
		}
		else if(curr!=NULL)									//for deleting any  intermediate node
		{
			q=curr;
			curr->prev->next=curr->next;
			curr->next->prev=curr->prev;
			curr=curr->next;
			delete q;										//free memory by deleting node
		}
	}
}

/**
 * Function(boolean) to seek next song in playlist.
 *
 * This function checks if the playlist is empty and if not, serves other functions
 * to make the right actions possible. If the next song is available, it assigns current 
 * pointer to that song and returns boolean values 'true' or 'false'.
 */
bool DoublyLinkedList::next()
{
	if(!empty() && curr!=tail)
	{
		curr=curr->next;
		return true;
	}
	else
		return false;
}

/**
 * Function(boolean) to seek previous song in playlist.
 *
 * This function checks if the playlist is empty and if not, serves other functions
 * to make the right actions possible. If the previous song is available, it assigns  
 * current pointer to that song and returns boolean values 'true' or 'false'.
 */
bool DoublyLinkedList::prev()								
{
	if(!empty() && curr!=head)
	{
		curr=curr->prev;
		return true;
	}
	else
		return false;
}

/**
 * Function(bool) to seek specified song in playlist.
 *
 * This function takes a string as reference and tries to match it with songs in
 * the playlist by traversing, if there are any. If found, that song is assigned as 
 * current and the function returns true, else returns false
 */
bool DoublyLinkedList::find(string &s)
    {
        dnode *newnode=new dnode(s);
        dnode *temp=head;
        while(temp!=NULL)                                   //traversing nodes for finding given string
        {
            if(newnode->data->compare(*(temp->data))==0)	//comparing given string with current node
            {
				curr=temp;
                return true;
            }
            else
                temp=temp->next;							//next node for comparison
        }
        return false;
    }

/**
 * Function(constant) to return current song in playlist.
 *
 * On whichever song the current pointer is pointing at, this function
 * returns that song as string data.
 */
const string& DoublyLinkedList::getData()					//returning data string in current node
{
	return *(curr->data);
}

/**
 * Primary deconstructor
 *
 * This deconstructor traverses the playlist at the end of the program
 * and deletes all songs(nodes) present in the playlist(doubly linked list).
 */
DoublyLinkedList::~DoublyLinkedList()						//primary deconstructor to delete nodes at end of program
{
    dnode *q,*temp;
    q=head;

    while(q!=NULL)
    {
        temp=q->next;
        delete q;											//free memory by deleting node
        q=temp;
    }
}

/**
 * Node deconstructor
 *
 * This deconstructor serves the vital function of freeing the memory
 * heap of all nodes in the doubly linked list at end of program.
 */
DoublyLinkedList::dnode::~dnode()							//deconstructor to deallocate memory
{
    prev=NULL;
    data=NULL;
	next=NULL;
}
