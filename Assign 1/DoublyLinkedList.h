/**
 * @file DoublyLinkedList.h   Implementation of a DoublyLinkedList header file for the same class.
 * 
 * @brief
 *    Implementation of a doubly linked list for a music playlist. A doubly linked list is
 *    used to store the song names.
 *
 * @author Prataprao Ughade
 * @date 09/13/13
 */
#ifndef DOUBLYLINKEDLIST_H
#define DOUBLYLINKEDLIST_H
#include <iostream>
#include <string>

using namespace std;

class DoublyLinkedList
{
	private:
		class dnode											//nested node class declaration
		{
			public:
			dnode(string&);									//parametrized node constructor for passing by reference
			dnode();										//simple node constructor for initialization
			~dnode();										//node deconstructor to deallocate memory
			dnode *prev;									//node pointer to prevous node
			string *data;									//node data pointer
			dnode *next;									//node pointer to next node
		};
		dnode *curr;										//node pointer for current node
		dnode *head;										//node pointer for first node
		dnode *tail;										//node pointer for last node
	public:
	    DoublyLinkedList();									//primary constructoe for intiialization
	    ~DoublyLinkedList();								//primary deconstructor utilized to delete nodes
	    void append(string&);								//function to add songs at end of list
	    bool empty();										//boolean function to check if list is empty
	    void insertBefore(string&);							//function to insert given string before current node
		void insertAfter(string&);							//function to insert given string after current node
		void begin();										//function to go to first song
		void end();											//function to go to last song
		bool next();										//boolean function to go to next song if list is not empty
		bool prev();										//boolean function to go to previous song if list is not empty
	    bool find(string &s);								//boolean function to check if given string is present in list
	    void remove(string&);								//function to delete specified node of it exists
		const string& getData();							//constant function to retrieve data if present

};						

#endif                                                     //end of class
