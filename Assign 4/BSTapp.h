/**
 * @file BSTapp.h   Implementation of a Binary Search Tree app header file  
 *
 * @brief
 *    Implementation of a binary search tree app header file for BSTapp source file
 *
 * @author Prataprao Ughade
 * @date 11/29/13
 */

#ifndef CSCI_311_BSTAPP_H
#define CSCI_311_BSTAPP_H
#include "BST.h"
#include <string>
using namespace std;

class BSTapp 
{
	public:
   		void mainLoop();						//declaration of main function extension
	private:
   		BST tree;							//data stored in a binary search tree
	   	void processCommand();					//declaration of command processing function
	   	void processInsert(string& key, string& data);		//declaration of inserting function
	   	void processFind(string&);					//declaration of finding function
	   	void processDelete(string&, string&);    			//declaration of deletion function
	   	void processPrint();						//declaration of printing function
	   	void processQuit();						//declaration of quit function
};

#endif