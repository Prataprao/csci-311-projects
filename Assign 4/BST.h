/**
 * @file BST.h   Binary Search Tree header file for BST class
 *
 * @brief
 *    Implementation of a binary search tree header file for the BST class
 *
 * @author Prataprao Ughade
 * @date 11/29/13
 */

#ifndef CSCI_311_BST_H
#define CSCI_311_BST_H
#include "Node.h"
#include <string>
using namespace std;

class BST {
public:
	void Tree_Insert(string&, string&);		//declaration for inserting nodes in tree
   	void Tree_Find(Node*, string&);			//declaration for finding nodes in tree	
	void Tree_Delete(Node*);				//declaration for deleting nodes in tree
	void Tree_Print();					//declaration for printing nodes in tree
	void Tree_Quit();					//declaration for quitting program systematically
	void Tree_Find_Delete(Node*, string&, string&);  //declaration for helping function to find duplicates to delete 
	Node* Root();						//declaration for wrapper function to return private members
	BST();							//declaration of constructor
	~BST();						//declaration of destructor
private:
	Node* root;						//initial declaration of root of tree
	Node* Tree_Minimum(Node*);				//function declaration to find minimum of node
	void Transplant(Node*, Node*);			//function declaration to perform transplant on tree
	void Inorder_Tree_Walk(Node*, int);		//function declaration to perform in-order traversal
};

#endif