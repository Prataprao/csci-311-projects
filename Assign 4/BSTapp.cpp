/**
 * @file BSTapp.cpp   Implementation of a Binary Search Tree app class  
 *
 * @brief
 *    Implementation of a binary search tree for calling correct functions as per requirement
 *
 * @author Prataprao Ughade
 * @date 11/29/13
 */

#include <iostream>
#include <stdlib.h>
#include <string>
#include "BSTapp.h"

using namespace std;

void BSTapp::mainLoop() 						//main loop extension to continue looking for commands
{						
	while (!cin.eof()) processCommand();
}

void BSTapp::processCommand()					//function to process commands as per requirement to call right function
{
	string s,command,key,data;
	getline(cin, s);
	command = s.substr(0, s.find(' '));
	s.erase(0, s.find(' ')+1);
	key = s.substr(0, s.find(' '));
	s.erase(0, s.find(' ')+1);
	data = s;
	if (command == "insert") processInsert(key, data);
	else if (command == "find") processFind(key);
	else if (command == "delete") processDelete(key, data);
	else if (command == "print") processPrint();
	else if (command == "quit") processQuit();
}

void BSTapp::processInsert(string& key, string& data)		//function to call insert function from BST class
{
	tree.Tree_Insert(key, data);
}

void BSTapp::processFind(string& key)				//function to call find function from BST class
{
	tree.Tree_Find(tree.Root(), key); 
}

void BSTapp::processDelete(string& key, string& data)		//function to call delete function from BST class
{
	tree.Tree_Find_Delete(tree.Root(), key, data);
}

void BSTapp::processPrint()						//function to call print function from BST class
{
	tree.Tree_Print();
}

void BSTapp::processQuit()						//function to call quit function from BST class
{
	tree.Tree_Quit();
	exit(0);
}

int main()								//main function to initiate program and continue execution
{
   	BSTapp myBST;
	myBST.mainLoop();
	return 0;
}

