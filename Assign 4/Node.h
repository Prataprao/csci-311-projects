/**
 * @file Node.h   Implementation of Node class header with appropriate properties
 *
 * @brief
 *    Implementation of a Node class header with appropriate declarations as per requirement
 *
 * @author Prataprao Ughade
 * @date 11/29/13
 */

class Node				//declaration for class with right member declarations for pointers
{
	friend class BST;
	private:
		Node* left;
        	Node* right;
		Node* parent;
		std::string* data;
		std::string* key;
       	Node();
	       ~Node();
};
