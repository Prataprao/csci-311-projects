/**
 * @file BST.cpp   Implementation of a Binary Search Tree class for manipulating data in a binary search tree
 *
 * @brief
 *    Implementation of a binary search tree for manipulating data in a binary search tree. A tree structure
 * is used to store the nodes.
 *
 * @author Prataprao Ughade
 * @date 11/29/13
 */

#include <string>
#include <iostream>
#include "BST.h"

using namespace std;

BST::BST()								//Constructor for BST class
{
	root = NULL;
}

Node* BST::Root()							//wrapper function to access private members from BSTapp.cpp
{
	return root;
}

void BST::Tree_Insert(string& key, string& data)			//Insert function to insert nodes inside the tree
{
	Node* y=NULL;
	Node* z=new Node;
	*z->data=data;
	*z->key=key;
	Node* x=root;
	while (x != NULL)
	{
		y=x;
		if (*z->key<*x->key)
			x=x->left;
		else x=x->right;
	}
	z->parent=y;
	if (y==NULL)
		root=z;
	else if (*z->key<*y->key)
		y->left=z;
	else y->right=z;
}

void BST::Tree_Find(Node* x, string& key)				//Find function to search and print matching nodes
{
	if (x != NULL && key==*x->key)
	{
		cout<< *x->key <<" "<< *x->data <<"\n";
		Tree_Find(x->right, key);				//Re-iteration to search for duplicates
		return;
	}
	if (x != NULL && key<*x->key)
		return Tree_Find(x->left, key);
	else if (x!=NULL) return Tree_Find(x->right, key);
}

void BST::Tree_Find_Delete(Node* x, string& key, string& data)	//Helper function to find and delete matching nodes
{
	if (x != NULL && key==*x->key && data==*x->data)
	{
		Tree_Delete(x);
		Tree_Find_Delete(root, key, data);			//Find duplicates to delete
		return;
	}
	if (x != NULL && key<*x->key)
		return Tree_Find_Delete(x->left, key, data);
	else if (x != NULL) return Tree_Find_Delete(x->right, key, data);
}

void BST::Tree_Delete(Node* z)					//delete function to check if the correct node is being deleted
{
	if (z->left==NULL)
		Transplant(z, z->right);
	else if (z->right==NULL)
		Transplant(z, z->left);
	else 
	{
		Node* y = Tree_Minimum(z->right);
		if(y->parent!=z)
		{
			Transplant(y, y->right);
			y->right=z->right;
			y->right->parent=y;
		}
		Transplant(z,y);
		y->left=z->left;
		y->left->parent=y;
	}
	delete z;
}

void BST::Transplant(Node* u, Node* v)				//transplant function to correct tree structure after deletion
{
	if(u->parent==NULL)
		root=v;
	else if (u==u->parent->left)
		u->parent->left=v;
	else u->parent->right=v;
	if (v != NULL)
		v->parent=u->parent;
}

Node* BST::Tree_Minimum(Node* x)					//function to find minimum node with no left child
{
	while(x->left != NULL)
		x=x->left;
	return x;
}

void BST::Tree_Print()						//function to call inorder tree walk to print as per command
{
	int i=0;
	Inorder_Tree_Walk(root,i);
}

void BST::Inorder_Tree_Walk(Node* p, int i)			//traverses the tree in-order and prints out nodes from tree  
{									//in ascending order
	if(p != NULL)
    	{
		i++;
		if(p->left) 
		{
			Inorder_Tree_Walk(p->left,i);
		}
        	cout<<i-1<<" "<<*p->key<<" "<<*p->data<<"\n";
        	if(p->right)
		{
			Inorder_Tree_Walk(p->right,i);
			i--;
		}
    	}
}

void BST::Tree_Quit()						//function to call destructor for tree
{
	this->~BST();
}

BST::~BST()								//destructor to delete tree methodically
{
	while(root != NULL)
		Tree_Delete(root);
}
