/**
 * @file Node.cpp   Implementation of Node class with appropriate properties
 *
 * @brief
 *    Implementation of a Node class with appropriate functions as per requirement
 *
 * @author Prataprao Ughade
 * @date 11/29/13
 */

#include <string>
#include "Node.h"

using namespace std;

Node::Node()				//call for constructor function for node initializing requisite pointers
{
	this->left=NULL;
	this->right=NULL;
	this->data=new string;
	this->parent=NULL;
	this->key=new string;
}

Node::~Node()				//call for deconstructor function for node destroying requisite pointers
{
	delete key;
    	key = NULL;
    	delete data;
      	data= NULL;
       left= NULL;
    	right= NULL;
   	parent = NULL;
}