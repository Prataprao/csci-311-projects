
/**
 * @file hash.cpp
 *
 * This file contains the code for importing data from a text file and processing it
 * into a hash table. It also contains functions that help the main.cpp file to print,
 * display,search or remove words from the hash table created after processing it.
 * There also exists a function to provide statistics on how well the hash table is 
 * created and maintained.
 * 
 * @author Prataprao Ughade
 * @date 11/08/13
 */

#include <string>
#include <list>
#include <iostream>
#include <fstream>
#include "hash.h"

using namespace std;

void Hash::processFile(string filename)						
{
	ifstream file(filename.c_str());
	std::string word;
	collisions = 0;
	avgLength = 0;
	longestList = 0;
	while (file >> word)
		{
			int key = this -> hf (word);				//find hash function key for each word in text file
			if ( !hashTable[key].empty())				//find collisions in each list if they take place
				collisions++;
			hashTable[key].push_back(word);				//insert word into hash table
			
			int x = hashTable[key].size();				//block of code to find longest list in hash table
			if ( x > longestList )
				longestList = x;

			double totalLength = 0;					//block of code to find average length of lists in hash table
			for (int i = 0 ; i < HASH_TABLE_SIZE ; i++)
				totalLength = totalLength + hashTable[i].size();
			double newAvgLength = totalLength / HASH_TABLE_SIZE;
			avgLength = (newAvgLength + avgLength) / 2.0;		
		}
		file.close();
}

void Hash::print()
{
	for (int i = 0 ; i < HASH_TABLE_SIZE ; i++)				//simple function to print hash table
    	{
       	cout << i << ":\t";
        	for (list<string>::iterator it = hashTable[i].begin() ; it != hashTable[i].end() ; ++it)
        	{   
			cout << *it << ", ";
	 	}
        	cout << endl;
    	}
}

bool Hash::search(string word)
{
	for (int i = 0 ; i < HASH_TABLE_SIZE ; i++)				//function to trace every list in hash table to find given word 
    	{										//and return boolean value
       	for (list<string>::iterator it=hashTable[i].begin(); it!=hashTable[i].end() ; ++it)
       	{   
			if (*it == word)
				return true;
		}
	}
	return false;
}

void Hash::remove(string word)							//function to search for given word and delete it from hash table
{
	for (int i = 0 ; i < HASH_TABLE_SIZE ; i++)
    	{
       	for (list<string>::iterator it = hashTable[i].begin() ; it!=hashTable[i].end() ; ++it)
        	{   
			if (*it == word)
			{
				hashTable[i].erase(it);
				
				double totalLength=0;				//block of code to recalculate average list length for hash table
				for (int i = 0 ; i < HASH_TABLE_SIZE ; i++)
					totalLength = totalLength + hashTable[i].size();
				double newAvgLength = totalLength / HASH_TABLE_SIZE;
				avgLength = (newAvgLength + avgLength) / 2.0;
		
				break;
			}
		}
	}
}

void Hash::output(string filename)							//function to print output to a file
{
	ofstream out(filename.c_str());
	for (int i = 0 ; i < HASH_TABLE_SIZE ; i++)
	{
       	out << i << ":\t";
        	for (list<string>::iterator it=hashTable[i].begin() ; it!=hashTable[i].end() ; ++it)
        	{   
			out << *it << ", ";
		}
        	out << endl;
    	}
}

void Hash::printStats()								//function to print statistics of efficiency of hash table
{
	cout << "Total collisions = " << collisions << endl;
	cout << "Longest list = " << longestList << endl;
	cout << "Average list length = " << avgLength << endl;
}
