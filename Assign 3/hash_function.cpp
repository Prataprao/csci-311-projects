/* This assignment originated at UC Riverside.*/

/**
 * @file hash_function.cpp   Code for the hash function.
 *
 * This particular function is a result of an expansive web search and some individual tweaking
 * done to optimize the function to create a table with optimum number of collisions and list 
 * length. Some of the function has been derived from lectures such as the link provided below
 * 
 * http://see.stanford.edu/player/SEEslplayer.aspx?coll=11f4f422-5670-4b4c-889c-008262e09e4e&co=a6223636-4c00-4175-a9c1-6eb2859a2dc8&sl=true
 *
 * My hash function is a combination of the concepts above and the hashing study made by Fowler-Noll-Vo
 * The numbers utilized by the FNV hash and so, implemented into mine, utilize the data consistency well,
 * to give and efficient hash table for any bucket size.
 *
 * Maintaining a good collision rate, my hash function gives an excellent maximum length for lists at 
 * individual hash table indexes.
 * A reference to the work of Fowler-Noll-Vo is provided below
 *
 * http://en.wikipedia.org/wiki/Fowler_Noll_Vo_hash
 *
 *http://www.isthe.com/chongo/tech/comp/fnv/
 *
 * @author Prataprao Ughade
 * @date 11/08/13
 */
 
#include <string>
#include "hash.h"

using namespace std;

int 
Hash::hf ( string ins ) {
  
  // place your hash function here....
 
    long int h = 2166136261;				//numbers referenced above as FNV members

    for (unsigned long i = 0; i < ins.length(); i++)
        h = ( h * 16777619 ) ^ ins[ i ];			//traverse string letter by letter to accumulate hash map value

    h %= HASH_TABLE_SIZE;					//adjust hash function value with bucket size or hash table size
    
    if (h < 0)
    	h += HASH_TABLE_SIZE;
    return h;

}

